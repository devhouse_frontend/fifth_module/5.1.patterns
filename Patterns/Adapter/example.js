// структура старой библиотеки
class ExampleLib {
    getLibName() {
        return 'I am ExampleLib';
    }

    calcDistance(a, b) {
        // ...
    }

    getCoordinatesByAddress(address) {
        // ...
    }

    getAddressByCoordinates(x, y) {
        // ...
    }

    // и т д
};

// для ее использования в программе применялась фабрика:
function getExampleLib() {
    return new ExampleLib();
};

// структура новой библиотеки
class NewSuperLib {
    myName() {
        return 'I am NewSuperLib';
    }

    getDistance(a, b, accuracy) {
        // ...
    }

    addressToCordinates(address, country, planet) {
        // ...
    }

    cordinatesToAddress(x, y, defaultAddress) {
        // ...
    }

    // и т д
};

// Применим паттерн адаптер:
class LibAdapter extends ExampleLib {
    // должна наследовать интерфейс от ExampleLib
    // и переопределить каждый ее метод

    constructor() {
        super()
        // инициализируем новую библиотеку для делегирования
        this._newSuperLib = new NewSuperLib();
    }

    getLibName() {
        return this._newSuperLib.myName();
    }

    calcDistance(a, b) {
        this._newSuperLib.getDistance(a, b, '0.0001');
    }

    getCoordinatesByAddress(address) {
        this._newSuperLib.addressToCordinates(address, 'Russia', 'Earth');
    }

    getAddressByCoordinates(x, y) {
        this._newSuperLib.cordinatesToAddress(x, y, '');
    }

    // и т д
};

// Теперь доработаем фабрику
function getExampleLib() {
    // подменяем старую библиотеку адаптером к новой
    return new LibAdapter();
};

// остальной код приложения менять не нужно
console.log(getExampleLib().getLibName());