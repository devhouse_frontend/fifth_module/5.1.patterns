// getItems - получить список предметов с сервера
// getItemDescription - получить описание конкретного предмета по его id

class APIConnector {
    constructor(apiUrl) {
        // apiUrl - адрес апи сервера, например https://api.example.com/api/v1
        this.apiUrl = apiUrl;
    }

    async getItems() {
        const response = await fetch(this.apiUrl + '/items');
        return await response.json();
    }

    async getItemDescription(itemId) {
        const response = await fetch(this.apiUrl + `/items/${itemId}`);
        const itemData = await response.json();
        return itemData ? itemData.description : '';
    }
};

// при работе с Webpack, как известно, он выставляет переменную среды окружения NODE_ENV
// = development при локальной разработке
// = production при сборке

// реальный APIConnector при process.env.NODE_ENV === 'production'
// и фейковый (заглушку) при process.env.NODE_ENV === 'development'

// первый способ сделать неправильно:
class APIConnector {
    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    async getItems() {
        if (process.env.NODE_ENV === 'development') {
            return [
                { id: 0, name: 'Тестовый item 1' },
                { id: 1, name: 'Тестовый item 2' },
            ];
        };
        const response = await fetch(this.apiUrl + '/items');
        return await response.json();
    }

    async getItemDescription(itemId) {
        if (process.env.NODE_ENV === 'development') {
            return 'Некоторое описание предмета из заглушки.';
        };
        const response = await fetch(this.apiUrl + `/items/${itemId}`);
        const itemData = await response.json();
        return itemData ? itemData.description : '';
    }
};


// через класс заглушки и фабричную функцию
// сам класс заглушки (исходный APIConnector остается без изменений)
class APIConnectorStub {
    async getItems() {
        return [
            { id: 0, name: 'Тестовый item 1' },
            { id: 1, name: 'Тестовый item 2' },
        ];
    }

    async getItemDescription(itemId) {
        return `Некоторое описание предмета ${itemId} из заглушки.`;
    }
};

// фабрика
function getApiConnector(apiUrl) {
    return process.env.NODE_ENV === 'development' ? new APIConnectorStub() : new APIConnector(apiUrl);
};

// проверяем
console.log(getApiConnector('https://api.example.com/api/v1'));