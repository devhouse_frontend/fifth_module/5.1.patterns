// допустим есть функция для поиска и красивого вывода элемента в массиве
const cars = [
    { id: 0, name: 'Lada' },
    { id: 1, name: 'BMW' },
    { id: 2, name: 'Toyota' },
];

function findCar(carId) {
    const car = cars.find(c => c.id === carId);
    if (car) {
        console.log('------');
        console.log(`Автомобиль - ${car.name}`);
        console.log('------');
    }
};

// метод для вывода можно вынести
function printCar(car) {
    console.log('------');
    console.log(`Автомобиль - ${car.name}`);
    console.log('------');
};

function findCar(carId) {
    const car = cars.find(c => c.id === carId);
    if (car) {
        printCar(car);
    }
};
